import React from 'react';
import {
  View, StyleSheet, ScrollView, Text,
} from 'react-native';
import MyTypes from 'MyTypes';
import { connect } from 'react-redux';
import { actionTypes } from '../actions';
import Header from '../components/Header';
import ListItem from '../components/ListItem';

const ListScreen = ({todoList, deleteToDo}) => {
  const handleDeleteButtonClick = (idx: number) => {
    deleteToDo(idx);
  };
  return (
    <View style={styles.wrapper}>
      <Header title="To-Do List" />
      <ScrollView>
        {!todoList.length ? <Text>Empty To-Do List</Text>
          : todoList.map((item, idx) => (
            <ListItem item={item} key={idx} idx={idx} handleDelete={handleDeleteButtonClick} />
          ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: 30,
    padding: 18,
  },
});

const MapStateToProps = (store: MyTypes.ReducerState) => ({
  count: store.todo.count,
  todoList: store.todo.list,
});

const MapDispatchToProps = (dispatch: Dispatch<MyTypes.RootAction>) => ({
  addToDo: (item: string) => dispatch({ type: actionTypes.ADD, payload: item }),
  deleteToDo: (idx: number) => dispatch({ type: actionTypes.DELETE, payload: idx }),
});

export default connect(
  MapStateToProps,
  MapDispatchToProps,
)(ListScreen);

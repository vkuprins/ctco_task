import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import { connect } from 'react-redux';
import MyTypes from 'MyTypes';
import { useNavigation } from '@react-navigation/native';
import Header from '../components/Header';
import { actionTypes } from '../actions';

const AddScreen = ({ addToDo }) => {
  const [title, setTitle] = useState('');
  const [text, setText] = useState('');
  const navigation = useNavigation();

  const handleButtonClick = () => {
    addToDo(title, text);
    navigation.navigate('ListScreen');
  };

  return (
    <View style={styles.wrapper}>
      <Header title="Add New To-Do" />
      <View style={{ marginTop: 30 }}>
        <TextInput
          selectionColor="#0091EA"
          underlineColor="#0091EA"
          mode="outlined"
          label="To-Do topic"
          value={title}
          onChangeText={(text) => setTitle(text)}
        />
        <TextInput
          selectionColor="#0091EA"
          underlineColor="#0091EA"
          style={{ marginTop: 20 }}
          mode="outlined"
          label="To-Do description"
          value={text}
          onChangeText={(text) => setText(text)}
        />
        <Button onPress={handleButtonClick} style={{ marginTop: 30, width: 100, alignSelf: 'flex-end' }} color="#0091EA" mode="contained">Add</Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: 30,
    padding: 18,
  },
});

const MapStateToProps = (store: MyTypes.ReducerState) => ({
  count: store.todo.count,
  todoList: store.todo.list,
});

const MapDispatchToProps = (dispatch: Dispatch<MyTypes.RootAction>) => ({
  addToDo: (...item: []) => dispatch({ type: actionTypes.ADD, payload: item }),
  deleteToDo: (idx: number) => dispatch({ type: actionTypes.DELETE, payload: idx }),
});

export default connect(
  MapStateToProps,
  MapDispatchToProps,
)(AddScreen);

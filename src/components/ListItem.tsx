import React from 'react';
import {
  Surface, Text, Button,
} from 'react-native-paper';
import { StyleSheet, View } from 'react-native';

interface ListItemProps {
  item: string;
  idx: number;
  handleDelete: (idx: number) => void;
}

const ListItem: React.FC<ListItemProps> = ({handleDelete, idx, item}) => (
  <Surface style={styles.surface}>
    <Text style={styles.textTitle}>
      {item[0]}
    </Text>
    <Text> {item[1]}</Text>
    <View style={{ alignItems: 'flex-end', marginTop: 20 }}>
      <Button onPress={() => handleDelete(idx)} labelStyle={styles.button}>Completed</Button>
    </View>
  </Surface>
);

const styles = StyleSheet.create({
  textTitle: {
    fontSize: 18,
    marginBottom: 22,
  },
  textDescription: {
    display: 'flex',
    alignItems: 'center',
  },
  surface: {
    padding: 20,
    elevation: 6,
    marginBottom: 30,
  },
  button: {
    fontSize: 14,
    color: '#0091EA',
  },
});

export default ListItem;

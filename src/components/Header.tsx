import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

const Header = ({ title }: any) => (
  <View style={styles.header_footer_style}>
    <Text style={styles.textStyle}>
      {' '}
      { title }
      {' '}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  header_footer_style: {
    marginTop: 50, // ToDo refactor this
    marginBottom: 20,
    width: '100%',
    height: 45,
    backgroundColor: '#fff',
  },
  textStyle: {
    textAlign: 'center',
    color: '#000000',
    fontSize: 18,
    padding: 7,
    fontWeight: 'bold',
  },
});

export default Header;

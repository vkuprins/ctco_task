import React from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import ListScreen from './src/screens/ListScreen';
import AddScreen from './src/screens/AddScreen';
import store from './src/store/store';

const Tab = createBottomTabNavigator();

export default () => (
  <Provider store={store}>
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="Screen"
        tabBarOptions={{
          activeTintColor: '#0091EA',
        }}
      >
        <Tab.Screen
          name="ListScreen"
          component={ListScreen}
          options={{
            tabBarLabel: '',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="format-list-checkbox" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="AddScreen"
          component={AddScreen}
          options={{
            tabBarLabel: '',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="plus-circle"
                color={color}
                size={size}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  </Provider>
);
